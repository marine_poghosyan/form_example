<?php
function form_example_views_data() {
    $data['form_example']['table']['group'] = t('visitors table');

    $data['form_example']['table']['base'] = array(
        'field' => 'id', // This is the identifier field for the view.
        'title' => t('Example table'),
        'help' => t('Example table contains example content and can be related to nodes.'),
        'weight' => -10,
    );

    $data['form_example']['first_name'] = array(
        'title' => t('first name'),
        'help' => t('visitors first name'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE, // This is use by the table display plugin.
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['form_example']['last_name'] = array(
        'title' => t('last name'),
        'help' => t('visitors last name'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE, // This is use by the table display plugin.
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );

    $data['form_example']['counter'] = array(
        'title' => t('counter'),
        'help' => t('visitors sign count'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE, // This is use by the table display plugin.
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
            'handler' => 'views_handler_argument_string',
        ),
    );


    return $data;
}
?>